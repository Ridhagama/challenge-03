import {NavigationContainer} from '@react-navigation/native';
import React, {useEffect, useState} from 'react';
import Router from './router';
import NetInfo from '@react-native-community/netinfo';
import {NoInternet} from './component';

const App = () => {
  const [isOffline, setOfflineStatus] = useState(false);

  const removeNetInfo = () => {
    NetInfo.addEventListener(state => {
      const offline = !(state.isConnected && state.isInternetReachable);
      setOfflineStatus(offline);
    });
  };

  useEffect(() => {
    removeNetInfo();
    return () => {
      removeNetInfo();
    };
  }, []);

  return (
    <NavigationContainer>
      {isOffline ? <NoInternet /> : <Router />}
    </NavigationContainer>
  );
};

export default App;
