import {StyleSheet, Text, View, Image, StatusBar} from 'react-native';
import React, {useEffect} from 'react';
import {Logo} from '../../assets';

const Splash = ({navigation}) => {
  useEffect(() => {
    setTimeout(() => {
      navigation.replace('Home');
    });
  }, [5000]);

  return (
    <View style={styles.container}>
      <StatusBar backgroundColor="black" />
      <Image source={Logo} style={styles.Logo} />
      <Text style={styles.MovieApp}> MovieApp</Text>
      <Text style={styles.Name}>Gama</Text>
    </View>
  );
};

export default Splash;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  Logo: {
    width: 100,
    height: 100,
  },
  MovieApp: {
    fontSize: 14,
    color: 'black',
    fontWeight: 'bold',
  },
  Name: {
    fontSize: 14,
    position: 'absolute',
    bottom: 18,
  },
});
