import {
  StyleSheet,
  Text,
  View,
  Image,
  Dimensions,
  ScrollView,
  ActivityIndicator
} from 'react-native';
import React, {useEffect, useState} from 'react';
import {CardPoster, ListFilm} from '../../component';
import axios from 'axios';

const Home = ({navigation}) => {
  const [isLoading, setLoading] = useState(true);
  const [data, setData] = useState([]);

  useEffect(() => {
    getData();
  }, []);

  async function getData() {
    try {
      await axios
        .get('http://code.aldipee.com/api/v1/movies')
        .then(response => {
          setData(response.data.results);
          console.log('succes');
        });
    } catch (err) {
      console.error(err);
    } finally {
      setLoading(false);
    }
  }
  return (
    <View>
      {isLoading ? (
        <ActivityIndicator size={'large'} />
      ) : (
        <ScrollView>
          <Text style={styles.Recommended}>Recommended</Text>
          <ScrollView>
            <View style={styles.Recommended}>
              {data.map(item => {
                return (
                  <CardPoster key={item.id} source={{uri: item.poster_path}} />
                );
              })}
            </View>
          </ScrollView>
          <Text style={styles.Latest}> Latest Upload </Text>
          <View>
            {data.map(item => {
              return (
                <ListFilm
                  key={item.id}
                  title={item.title}
                  rating={item.vote_average}
                  date={item.release_date}
                  source={{uri: item.poster_path}}
                  onPress={() =>
                    navigation.navigate('MovieDetail', {id: item.id})
                  }
                />
              );
            })}
          </View>
        </ScrollView>
      )}
    </View>
  );
};

export default Home;

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

const styles = StyleSheet.create({
  page: {
    flex: 1,
    backgroundColor: 'violet',
    paddingVertical: 13,
    paddingHorizontal: 13,
  },
  Recommended: {
    flexDirection: 'row',
    margin: 8,
    fontSize: 18,
    fontWeight: 'bold',
  },
  Duncan: {
    height: windowHeight * 0.33,
    width: windowWidth * 0.4,
    borderRadius: 8,
  },
  spiderman: {
    height: windowHeight * 0.33,
    width: windowWidth * 0.4,
    borderRadius: 8,
  },
  Get: {
    height: windowHeight * 0.33,
    width: windowWidth * 0.4,
    borderRadius: 8,
  },
  Latest: {
    fontSize: 18,
    fontWeight: 'bold',
    marginTop: 8,
  },
});
