import Home from './home';
import Splash from './splash';
import MovieDetail from './movie detail';

export {Home, Splash, MovieDetail};
