import {
  StyleSheet,
  Text,
  View,
  Dimensions,
  ImageBackground,
  ScrollView,
  Image,
  Share,
} from 'react-native';
import React, {useState, useEffect} from 'react';
import {Button, CardPoster, ButtonIcon} from '../../component';
import axios from 'axios';
import { ICBack } from '../../assets';

const MovieDetail = ({navigation, route}) => {
  const {id} = route.params;
  console.log(id);
  const [isLoading, setLoading] = useState(true);
  const [data, setData] = useState([]);
  const [genres, setgenres] = useState([]);
  const [Actors, setactors] = useState([]);
  const [sinopsis, setsinopsis] = useState([]);

  useEffect(() => {
    getData();
  }, []);

  async function getData() {
    try {
      await axios
        .get('http://code.aldipee.com/api/v1/movies/' + id)
        .then(response => {
          setData(response.data);
          setgenres(response.data.genres);
          setactors(response.data.credits.cast);
          setsinopsis(response.data.overview);
          console.log(response.data);
        });
    } catch (err) {
      console.error(err);
    } finally {
      setLoading(false);
    }
  }

  const onShare = async () => {
    try {
      const result = await Share.share({
        message: `go to home for detail movies ${data.original_title} ${data.homepage}`,
      });

      if (result.action === Share.sharedAction) {
        if (result.activityType) {
          // shared with activity type of result.activityType
        } else {
          // shared
        }
      } else if (result.action === Share.dismissedAction) {
        // dismissed
      }
    } catch (error) {
      alert(error.message);
    }
  };

  return (
    <ScrollView>
      <View
        style={{
          height: windowHeight * 0.3,
          width: windowWidth,
          // backgroundColor: 'red',
        }}>
        <ImageBackground
          source={{uri: data.backdrop_path}}
          style={{height: windowHeight * 0.3, width: windowWidth}}>
          <View
            style={{
              // backgroundColor: 'white',
              flexDirection: 'row',
              justifyContent: 'space-between',
            }}>
            <View>
              <ButtonIcon name={'chevron-back'} onPress={() => navigation.goBack('Home')}/>
            </View>
            <View style={{flexDirection: 'row'}}>
              <ButtonIcon name={"heart"} />
              <ButtonIcon name={"share"} onPress={onShare} />
            </View>
          </View>
        </ImageBackground>
      </View>
      <View
        style={{
          height: windowHeight * 0.38,
          width: windowWidth,
          // backgroundColor: 'blue',
          flexDirection: 'row',
          padding: 15,
        }}>
        <View style={{flex: 1}}>
          <CardPoster source={{uri: data.poster_path}} />
        </View>
        <View
          style={{
            padding: 15,
            backgroundColor: 'white',
            flex: 1.1,
            alignItems: 'center',
          }}>
          <Text style={{marginBottom: 6}}>{data.original_title}</Text>
          <Text style={{marginBottom: 6}}>{data.tagline}</Text>
          <Text style={{marginBottom: 6}}>{data.status}</Text>
          <Text style={{marginBottom: 6}}>{data.release_date}</Text>
          <Text style={{marginBottom: 6}}>{data.vote_average}</Text>
          <Text style={{marginBottom: 6}}>{data.runtime}</Text>
        </View>
      </View>
      <View
        style={{
          width: windowWidth,
          padding: 15,
        }}>
        <Text style={{fontSize: 24, fontWeight: 'bold', marginBottom: 8}}>
          Genres
        </Text>

        {genres.map(item => {
          return <Text key={item.id}> {item.name} </Text>;
        })}
      </View>
      <View
        style={{
          width: windowWidth,
          padding: 15,
          backgroundColor: 'white',
          flex: 1.1,
          // alignItems: 'lef',
        }}>
        <Text style={{fontSize: 24, fontWeight: 'bold', marginBottom: 6}}>
          Sinopsis
        </Text>
        <Text>{data.overview}</Text>
      </View>
      <View
        style={{
          width: windowWidth,
        }}>
        <Text
          style={{
            fontSize: 24,
            fontWeight: 'bold',
            marginBottom: 8,
            padding: 15,
          }}>
          Actor
        </Text>
        <View
          style={{
            flexDirection: 'row',
            flexWrap: 'wrap',
            width: windowWidth,
            justifyContent: 'space-between',
            // backgroundColor: 'purple',
          }}>
          {Actors.map(item => {
            return (
              <View
                key={item.id}
                style={{
                  backgroundColor: 'white',
                  width: 120,
                  height: 250,
                  alignItems: 'center',
                  padding: 15,
                }}>
                <Image
                  source={{uri: item.profile_path}}
                  style={{
                    height: 200,
                    width: 100,
                    borderWidth: 1,
                    borderColor: 'black',
                  }}
                />
                <Text style={{textAlign: 'center'}}> {item.name}</Text>
              </View>
            );
          })}
        </View>
      </View>
    </ScrollView>
  );
};

export default MovieDetail;

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

const styles = StyleSheet.create({});
