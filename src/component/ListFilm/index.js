import {StyleSheet, Text, View, Image, Dimensions} from 'react-native';
import React from 'react';
import Button from '../Button';

const ListFilm = ({source, title, date, rating, onPress}) => {
  return (
    <View style={styles.container}>
      <Image source={source} style={styles.imagelist} />

      <View style={styles.description}>
        <Text style={styles.title}>{title}</Text>
        <Text>{date}</Text>
        <Text>{rating}</Text>
        <Button onPress={onPress} title={'show more'} />
      </View>
    </View>
  );
};
export default ListFilm;

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

const styles = StyleSheet.create({
  imagelist: {
    width: null,
    height: null,
    borderRadius: 11,
    marginRight: 16,
    flex: 2,
  },
  container: {
    flexDirection: 'row',
    marginTop: 16,
    width: windowWidth * 0.95,
    height: windowHeight * 0.3,
    backgroundColor: '#E9E9E9',
  },
  title: {
    fontSize: 20,
    fontWeight: '600',
    color: 'black',
  },
  date: {
    fontSize: 18,
  },
  rating: {
    fontSize: 18,
  },
  description: {
    flex: 4,
  },
});
