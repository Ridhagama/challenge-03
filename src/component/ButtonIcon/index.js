import React from 'react';
import { StyleSheet, TouchableOpacity, View, Text } from 'react-native';
import { ICBack, ICLike, ICShare } from '../../assets';
import Icon from 'react-native-vector-icons/Ionicons'

const ButtonIcon = ({ name, onPress }) => {

  return (
    <TouchableOpacity style={styles.iconWrapper} onPress={onPress}>
      <View style={styles.icon}>
        <Icon name={name} size={40} color='black'/>
      </View>
    </TouchableOpacity>

    
  );
};

export default ButtonIcon;

const styles = StyleSheet.create({
  iconWrapper: {
    width: 50,
    height:50,
    borderRadius: 50 / 2,
    opacity: 0.8,
    backgroundColor:'white'
  },
  icon: {
    // padding: 16,
    alignSelf: 'center',
    justifyContent:'center'
    // borderRadius: 8,
  },
});
