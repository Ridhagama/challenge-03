import { StyleSheet, Text, View, StatusBar } from 'react-native';
import React from 'react';
import { colors, fonts } from '../../utils';
import Button from '../Button';

const NoInternet = ({ onRetry, isRetrying }) => {
  return (
    <View style={styles.wrapper}>
      <StatusBar barStyle="light-content" backgroundColor={colors.background.primary} />
      <View style={styles.Container}>
        <Text style={styles.Title}>Connection Error</Text>
        <Text style={styles.Text}>
          Oops! Looks like your device is not connected to the Internet.
        </Text>
      </View>
    </View>
  );
};

export default NoInternet;

const styles = StyleSheet.create({
  wrapper: {
    flex: 1,
    position: 'absolute',
    alignItems: 'center',
  
    width: '100%',
    height: '100%',
    justifyContent: 'center',
  },

  Container: {
    
    paddingHorizontal: 16,
    paddingTop: 20,
    paddingBottom: 40,
  },
  Title: {
    fontSize: 22,
  },
  Text: {
    fontSize: 18,
    marginTop: 14,
 
  },
});
