import {StyleSheet, Text, View, Dimensions, Image} from 'react-native';
import React from 'react';

const CardPoster = ({source}) => {
  return (
    <View>
      <Image source={source} style={styles.Get}  />
    </View>
  );
};

export default CardPoster;

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

const styles = StyleSheet.create({
  Get: {
    height: windowHeight * 0.33,
    width: windowWidth * 0.4,
    borderRadius: 8,
    marginRight: 10,
  },
});
